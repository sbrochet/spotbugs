package utils

import (
	"flag"
	"os"
	"os/exec"
	"regexp"
	"testing"

	"github.com/urfave/cli/v2"
)

func TestSetupSystemDefaults(t *testing.T) {
	tests := []struct {
		name      string
		version   string
		supported bool
	}{
		{
			name:      "Using java 8",
			version:   "8",
			supported: false,
		},
		{
			name:      "Using java 11",
			version:   JavaVersion11,
			supported: true,
		},
		{
			name:      "Using java 17",
			version:   JavaVersion17,
			supported: true,
		},
		{
			name:      "Var not set, using java 17 as fallback",
			version:   "",
			supported: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := &cli.App{
				Writer: os.Stdout,
			}
			set := flag.NewFlagSet("", 0)
			set.String(FlagJavaVersion, tt.version, "")
			c := *cli.NewContext(app, set, nil)

			setup := SetupSystemJava(&c)
			switchVersion := tt.version
			if !tt.supported {
				switchVersion = DefaultJavaVersion // fallback to default version
			}

			re := regexp.MustCompile("switch_to java " + switchVersion)
			if re.FindStringSubmatch(setup.cmd.String()) == nil {
				t.Errorf("SetupSystemJava() %v - cmd string = %v", tt.name, setup.cmd.String())
				return
			}
		})
	}
}

func TestSetupSystemJavaUsingCustomPath(t *testing.T) {
	app := &cli.App{
		Writer: os.Stdout,
	}
	set := flag.NewFlagSet("", 0)
	set.String("javaPath", "/bin/java", "")
	c := *cli.NewContext(app, set, nil)

	setup := SetupSystemJava(&c)

	err := setup.Run()
	if err != nil {
		t.Errorf("SetupSystemJava() error = %v", err)
	}
}

func TestRunCmdWithTextErrorDetection(t *testing.T) {
	type args struct {
		cmd       *exec.Cmd
		errorText string
		message   string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Command success",
			args: args{
				cmd:       exec.Command("true"),
				errorText: "errorText",
				message:   "",
			},
			wantErr: false,
		},
		{
			name: "Command fails by text",
			args: args{
				cmd:       exec.Command("echo", "errorText"),
				errorText: "errorText",
				message:   "",
			},
			wantErr: true,
		},
		{
			name: "Command fails by exit code",
			args: args{
				cmd:       exec.Command("false"),
				errorText: "errorText",
				message:   "",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		c := &cli.Context{
			App: &cli.App{
				Writer: os.Stdout,
			},
		}
		t.Run(tt.name, func(t *testing.T) {
			err := RunCmdWithTextErrorDetection(tt.args.cmd, c, tt.args.errorText, tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("RunCmdWithTextErrorDetection() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
